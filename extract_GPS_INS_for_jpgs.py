#!/usr/bin/env python2.7

import xarray as xr
import numpy as np
import fnmatch
import os
import datetime as dt
import sys
from osgeo import ogr,osr
import cameratransform as camtrans
import pylab as plt
import pandas as pd
from import_export_geotiff import ImportGeoTiff

####Script to extract GPS and IMU data from netCDF files and generate 'Reference_data.txt' file for Agisoft metashape.
####Exif data of jpges in JPG folder will be updated for the use with OpenDroneMap if update_exif flag is set to 1.
####Shapefile of flight track is created including attribute table of relevant metadata.
####Geojson of study area is created to download respective Sentinel-2 data.
####Drift correction to the mid time of flight.
####N. Neckel 2022

CAMpath = sys.argv[1] #path to raw images
JPGpath = sys.argv[2] #path to jpg images
INSpath = sys.argv[3] #path to INS netCDFs
GPSpath = sys.argv[4] #path to GPS netCDFs
outname_init = sys.argv[5] #flight ID
update_exif = int(sys.argv[6]) #update exif flag
calc_drift_corr = int(sys.argv[7]) #calculate drift correction
calc_geotifs = int(sys.argv[8]) #calculate geotifs of single footprints
box_size = 3000
PScoords = np.load('PS_Trimble1_20200101_20210101_P2.npy',allow_pickle=True)
#PScoords = np.load('Sled_20200616_P2.npy',allow_pickle=True)

DEM,width,height,match_geotrans,match_proj = ImportGeoTiff("DTU21MSS_EPSG3413.tif")

if calc_geotifs == 1 and not os.path.exists("geotifs"):
	os.system("mkdir geotifs")

#intrinsic camera parameters
f = 1972.1978296708107 # in px
k1 = -0.022078639136642535
k2 = 0.021322004279031137
k3 = -0.014870182833038299
image_size = (3908, 2600)  # in px

####Transform coordinates between lat lon and polar stereo (ps)
srs_in = osr.SpatialReference()
srs_in.ImportFromEPSG(4326)
srs_out = osr.SpatialReference()
srs_out.ImportFromEPSG(3413)
ct = osr.CoordinateTransformation(srs_in,srs_out)

#####Transform coordinates between polar stereo (ps) and lat lon
srs_in_back = osr.SpatialReference()
srs_in_back.ImportFromEPSG(3413)
srs_out_back = osr.SpatialReference()
srs_out_back.ImportFromEPSG(4326)
ct_back = osr.CoordinateTransformation(srs_in_back,srs_out_back)

def EstimateFootprint(img,lat,lon,alt,head,tilt,roll):
	img = img[:-4]+".jpg"
	cam = camtrans.Camera(camtrans.RectilinearProjection(focallength_px=f,image=image_size),
camtrans.SpatialOrientation(elevation_m=alt,tilt_deg=tilt,roll_deg=roll,heading_deg=head),
camtrans.BrownLensDistortion(k1=k1,k2=k2,k3=k3,projection=camtrans.RectilinearProjection(focallength_px=f,image=image_size)))
	cam.setGPSpos(lat,lon,alt)
	coords = np.array([cam.gpsFromImage([0 , 0]), cam.gpsFromImage([image_size[0]-1 , 0]), cam.gpsFromImage([image_size[0]-1, image_size[1]-1]), cam.gpsFromImage([0 , image_size[1]-1]), cam.gpsFromImage([0 , 0])])
	ring = ogr.Geometry(ogr.wkbLinearRing)	
	for m in np.arange(len(coords)):
		psX,psY,psZ = ct.TransformPoint(coords[m][1],coords[m][0])
		ring.AddPoint(psX,psY)
	poly = ogr.Geometry(ogr.wkbPolygon)
	poly.AddGeometry(ring)
	if calc_geotifs == 1 and os.path.exists(JPGpath+"/"+img):
		im = plt.imread(JPGpath+"/"+img)
		unim = cam.undistortImage(im)
		plt.imsave(img[:-4]+"_undistort.jpg",unim.copy(order='C'),format="jpg")
		os.system("gdal_translate -gcp "+str(image_size[0])+" "+str(image_size[1])+" "+str(coords[0,1])+" "+str(coords[0,0])+" -gcp 0 "+str(image_size[1])+" "+str(coords[1,1])+" "+str(coords[1,0])+" -gcp 0 0 "+str(coords[2,1])+" "+str(coords[2,0])+" -gcp "+str(image_size[0])+" 0 "+str(coords[3,1])+" "+str(coords[3,0])+" "+img[:-4]+"_undistort.jpg tmp.tif -a_nodata 0")
		os.system("gdalwarp -tps -r bilinear -s_srs EPSG:4326 -t_srs EPSG:3413 -overwrite tmp.tif  geotifs/"+img[:-4]+".tif -dstnodata 0")
		os.system("rm "+img[:-4]+"_undistort.jpg tmp.tif")
	return poly

def FindStringBetweenSign(file,sign):
        value = file.split(sign)
        return value

def GetFileList(path,wildcard):
	filelist = []
	for file in os.listdir(path):
		if fnmatch.fnmatch(file, wildcard):
			filelist = np.append(filelist,file)
	return np.sort(filelist)

def interpolatePScoords(day):
	day = day+'000000'
	day = dt.datetime.strptime(day,'%Y%m%d%H%M%S')
	idx = PScoords[:,0].searchsorted(day)
	PSday = PScoords[idx:idx+1441]

	index = pd.DatetimeIndex(PSday[:,0])
	DTserieslat = pd.Series(PSday[:,1].astype(float),index=index)
	DTserieslon = pd.Series(PSday[:,2].astype(float),index=index)

	DTupSerieslat = DTserieslat.resample('1S')
	DTupSerieslon = DTserieslon.resample('1S')

	inttime = DTupSerieslat.interpolate(method='linear').index.to_pydatetime()
	intlat = DTupSerieslat.interpolate(method='linear').to_numpy()
	intlon = DTupSerieslon.interpolate(method='linear').to_numpy()

	return np.stack((inttime,intlat,intlon),axis=1)

def DriftCorr(PScoords,cameraGPSdate,psx,psy,PSpsX_init,PSpsY_init):
	idx = np.searchsorted(PScoords[:,0],cameraGPSdate)
	PSpsX,PSpsY,PSpsZ = ct.TransformPoint(PScoords[idx,2],PScoords[idx,1])
	PSFLIGHTLIST.write(str(PSpsX)+' '+str(PSpsY)+'\n')
	dx = PSpsX-PSpsX_init
	dy = PSpsY-PSpsY_init
	psx_corr = psx-dx
	psy_corr = psy-dy
	lon_corr,lat_corr,z_corr = ct_back.TransformPoint(psx_corr,psy_corr)
	return lat_corr,lon_corr,psx_corr,psy_corr

def FindGPSdata(GPSlist,cameraGPSdate,tags,CAMname):
	a = 0
	for g in np.arange(len(GPSlist)):
		date1 = dt.datetime.strptime(FindStringBetweenSign(GPSlist[g],"_")[3],'%Y%m%dT%H%M%S')
		date2 = dt.datetime.strptime(str(date1.year)+str(date1.month).zfill(2)+str(date1.day).zfill(2)+FindStringBetweenSign(GPSlist[g],"_")[4][:-3],'%Y%m%d%H%M%S')
		if date1 <= cameraGPSdate <= date2 and GPSlist[g].startswith("GPS_R"):
			print("Using precise GPS and INS data for "+CAMname[:-4]+".jpg. Extracting data from GPS file "+GPSlist[g]+".")
			a = a+1
			GPS_netCDF = xr.open_mfdataset(GPSpath+"/"+GPSlist[g])
			date = dt.datetime.strptime(FindStringBetweenSign(GPSlist[g],"_")[3][:8],'%Y%m%d')
			GPS_time = (GPS_netCDF['TIME'].values/1000).astype(int)
			GPS_lat = GPS_netCDF['LATITUDE'].values
			GPS_lon = GPS_netCDF['LONGITUDE'].values
			GPS_alt = GPS_netCDF['ALTITUDE'].values
			start = np.datetime64(str(date))
			GPS_date = np.datetime64(start) + GPS_time.astype('timedelta64[s]')
			idx = np.searchsorted(GPS_date,cameraGPSdate)
			GPS_lat = GPS_lat[idx]
			GPS_lon = GPS_lon[idx]
			GPS_alt = GPS_alt[idx]
			psx,psy,psz = ct.TransformPoint(GPS_lon,GPS_lat)
	if a == 0:
		print("WARNING: NO GPS data available for "+CAMname)
		GPS_lat = np.nan
		GPS_lon = np.nan
		GPS_alt = np.nan
		psx = np.nan
		psy = np.nan
	if a == 1 and update_exif == 1:
		os.system("exiftool -overwrite_original -GPSLatitude="+str(GPS_lat)+" "+JPGpath+"/"+CAMname[:-4]+".jpg")
		os.system("exiftool -overwrite_original -GPSLongitude="+str(GPS_lon)+" "+JPGpath+"/"+CAMname[:-4]+".jpg")
	return GPS_lat,GPS_lon,GPS_alt,psx,psy

def FindINSdata(INSlist,cameraGPSdate):
	b = 0
	for l in np.arange(len(INSlist)):
		date1 = dt.datetime.strptime(FindStringBetweenSign(INSlist[l],"_")[2],'%Y%m%dT%H%M%S')
		date2 = dt.datetime.strptime(str(date1.year)+str(date1.month).zfill(2)+str(date1.day).zfill(2)+FindStringBetweenSign(INSlist[l],"_")[3][:-3],'%Y%m%d%H%M%S')
		if date1 <= cameraGPSdate <= date2:
			b = b+1
			INS_netCDF = xr.open_mfdataset(INSpath+"/"+INSlist[l])
			date = dt.datetime.strptime(FindStringBetweenSign(INSlist[l],"_")[2][:8],'%Y%m%d')
			INS_time = (INS_netCDF['TIME'].values/1000).astype(int)
			INS_yaw = INS_netCDF['THDG'].values
			INS_pitch = INS_netCDF['PITCH'].values
			INS_roll = INS_netCDF['ROLL'].values
			start = np.datetime64(str(date))
			INS_date = np.datetime64(start) + INS_time.astype('timedelta64[s]')
			idx = np.searchsorted(INS_date,cameraGPSdate)
			INS_yaw = INS_yaw[idx]
			INS_pitch = INS_pitch[idx]
			INS_roll = INS_roll[idx]
	if b == 0:
		INS_yaw = np.nan
		INS_pitch = np.nan
		INS_roll = np.nan
	return INS_yaw,INS_pitch,INS_roll

def AppendPntList(pointlist,it,CAMname):
	pointlist = np.append(pointlist,it+1)
	pointlist = np.append(pointlist,GPS_lat)
	pointlist = np.append(pointlist,GPS_lon)
	pointlist = np.append(pointlist,GPS_alt)
	pointlist = np.append(pointlist,psx)
	pointlist = np.append(pointlist,psy)
	pointlist = np.append(pointlist,cameraGPSdate)
	pointlist = np.append(pointlist,CAMname)
	pointlist = np.append(pointlist,INS_yaw)
	pointlist = np.append(pointlist,INS_pitch)
	pointlist = np.append(pointlist,INS_roll)
	pointlist = np.append(pointlist,GPS_lat_orig)
	pointlist = np.append(pointlist,GPS_lon_orig)
	pointlist = np.append(pointlist,psx_orig)
	pointlist = np.append(pointlist,psy_orig)
	pointlist = np.append(pointlist,refALT)
	return pointlist

CAMlist = GetFileList(CAMpath,"*CR2")
INSlist = GetFileList(INSpath,"INS**nc")
GPSlist = GetFileList(GPSpath,"GPS**nc")

PScoords = interpolatePScoords(FindStringBetweenSign(outname_init,"_")[0])
pointlist = []
cameradatelist = []
PSFLIGHTLIST = open(outname_init+'_PS_coords.txt','w')
for i in np.arange(len(CAMlist)):
	CAMfile = CAMlist[i]
	if FindStringBetweenSign(CAMlist[i],"_")[0] != FindStringBetweenSign(outname_init,"_")[0]:
		#os.system("rm "+CAMpath+"/"+CAMfile)
		print("Warning "+CAMfile+" has a different date than: "+FindStringBetweenSign(outname_init,"_")[0])
	try:
		CAMname = FindStringBetweenSign(CAMlist[i],",")[0]+"_"+FindStringBetweenSign(CAMlist[i],",")[1]
	except:
		CAMname = CAMlist[i]
	tags = os.popen("exiftool -n -f "+CAMpath+"/"+CAMfile+" -CreateDate -GPSLatitude -GPSLongitude -GPSAltitude -GPSDateTime").readlines()            
	try:
		cameraGPSdate = dt.datetime.strptime(FindStringBetweenSign(CAMname,"_")[0]+FindStringBetweenSign(CAMname,"_")[1],'%Y%m%d%H%M%S')
	except:
		if tags[0].split()[3] != "-":
			if tags[4].split()[4].endswith("Z"):
				GPStime = tags[4].split()[4][:-1]
				GPStime = GPStime.split(".")[0]
			else:
				GPStime = tags[4].split()[4]
			cameradate = dt.datetime.strptime(tags[0].split()[3]+tags[0].split()[4],'%Y:%m:%d%H:%M:%S')
			if cameradate.date() != dt.datetime.strptime(tags[4].split()[3],'%Y:%m:%d').date():
				cameraGPSdate = dt.datetime.strptime(str(cameradate.date())+GPStime, '%Y-%m-%d%H:%M:%S')
			else:
				cameraGPSdate = dt.datetime.strptime(tags[4].split()[3]+GPStime,'%Y:%m:%d%H:%M:%S')
	GPS_lat_orig,GPS_lon_orig,GPS_alt,psx_orig,psy_orig = FindGPSdata(GPSlist,cameraGPSdate,tags,CAMname)
	INS_yaw,INS_pitch,INS_roll = FindINSdata(INSlist,cameraGPSdate)
	if i == 0:
		INDEX = int(len(CAMlist)/2)
		initCAMname = CAMlist[INDEX]
		initcameraGPSdate = dt.datetime.strptime(FindStringBetweenSign(initCAMname,"_")[0]+FindStringBetweenSign(initCAMname,"_")[1],'%Y%m%d%H%M%S')	
		idx = np.searchsorted(PScoords[:,0],initcameraGPSdate)
		PSpsX_init,PSpsY_init,PSpsZ_init = ct.TransformPoint(PScoords[idx,2],PScoords[idx,1])
	if calc_drift_corr == 1:
		GPS_lat,GPS_lon,psx,psy = DriftCorr(PScoords,cameraGPSdate,psx_orig,psy_orig,PSpsX_init,PSpsY_init)
	else:
		GPS_lat,GPS_lon = GPS_lat_orig,GPS_lon_orig
		psx,psy = psx_orig,psy_orig
	if str(GPS_lat) != "inf":
		xcoord = int(((psx - match_geotrans[0]) / match_geotrans[1]))
		ycoord = int(((psy - match_geotrans[3]) / match_geotrans[5]))
		refALT = DEM[ycoord,xcoord]
		pointlist = AppendPntList(pointlist,i,CAMname)
	else:
		#os.system("rm "+CAMpath+"/"+CAMfile)
		print("Warning no GPS and INS data found for "+CAMfile+".")
PSFLIGHTLIST.close()
pointlist = pointlist.reshape(int(len(pointlist)/16),16)

headfile = open("heading.txt","w")
distlist = [0]
bearinglist_corr = [0]
bearinglist_orig = [0]
bearingdifflist = [0]
for i in np.arange(len(pointlist)-1):
	if ~np.isnan(pointlist[i,4]):
		dist = np.sqrt((pointlist[i,4]-pointlist[i+1,4])**2+(pointlist[i,5]-pointlist[i+1,5])**2)
		distlist = np.append(distlist,round(dist,2))
		bearing_corr = camtrans.getBearing([pointlist[i,1], pointlist[i,2]], [pointlist[i+1,1], pointlist[i+1,2]])
		bearinglist_corr = np.append(bearinglist_corr,bearing_corr)
		bearing_orig = camtrans.getBearing([pointlist[i,11], pointlist[i,12]], [pointlist[i+1,11], pointlist[i+1,12]])
		bearinglist_orig = np.append(bearinglist_orig,bearing_orig)
		bearingdiff = np.rad2deg(np.arctan2(np.sin(np.deg2rad(bearing_corr)-np.deg2rad(bearing_orig)), np.cos(np.deg2rad(bearing_corr)-np.deg2rad(bearing_orig))))
		bearingdifflist = np.append(bearingdifflist,bearingdiff)
		headfile.write(str(pointlist[i,7])+" "+str(pointlist[i,8])+" "+str(bearing_orig)+" "+str(bearing_corr)+" "+str(bearingdiff)+"\n")
distlist = np.cumsum(distlist)
headfile.close()

timediff = np.nanmax(pointlist[:,6])-np.nanmin(pointlist[:,6])
vel = (np.nanmax(distlist)/timediff.total_seconds())*3.6
alt = np.nanmean(pointlist[:,3].astype(float))
summary = open(outname_init+"_summary.txt","w")
summary.write("#####################flight summary#####################\n")
summary.write("###average flight velocity: "+str(round(vel,1))+" km/h.\n")
summary.write("###average flight altitude: "+str(round(alt,1))+" masl.\n")
summary.write("###flight time: "+str(timediff)+" H:M:S.\n")
summary.write("###start time: "+np.nanmin(pointlist[:,6]).strftime('%H:%M:%S')+" H:M:S.\n")
summary.write("###drift correction time: "+initcameraGPSdate.strftime('%H:%M:%S')+" H:M:S.\n")
summary.write("###stop time: "+np.nanmax(pointlist[:,6]).strftime('%H:%M:%S')+" H:M:S.\n")
summary.write("###flight distance: "+str(round(np.nanmax(distlist)/1000,1))+" km.\n")
summary.close()

if calc_drift_corr == 1:
	outname = outname_init+'_'+initcameraGPSdate.time().strftime("%H%M%S")+'_drift_corr' 
	driftlines = []
else:
	outname = outname_init

###Create Reference file for Agisoft
print('Write reference file: Reference_data.txt')
outfile = open(JPGpath+'/Reference_data.txt', 'w')
outfile.write('# filename; lat; lon; alt; yaw; pitch; roll; lat_orig; lon_orig; yaw_orig'+'\n')
###initial line shape file of flightpath
line = ogr.Geometry(ogr.wkbLineString)
###create output multipolygon footprint shapefile
dest_srs = ogr.osr.SpatialReference()
dest_srs.ImportFromEPSG(3413)
outShapefile = outname+'_footprints.shp'
outDriver = ogr.GetDriverByName('Esri Shapefile')
if os.path.exists(outShapefile):
	outDriver.DeleteDataSource(outShapefile)
outDataSource = outDriver.CreateDataSource(outShapefile)
layer = outDataSource.CreateLayer('', dest_srs, ogr.wkbMultiPolygon)
layer.CreateField(ogr.FieldDefn('image', ogr.OFTString))
layer.CreateField(ogr.FieldDefn('dist', ogr.OFTReal))
layer.CreateField(ogr.FieldDefn('time', ogr.OFTString))
layer.CreateField(ogr.FieldDefn('lat', ogr.OFTReal))
layer.CreateField(ogr.FieldDefn('lon', ogr.OFTReal))
layer.CreateField(ogr.FieldDefn('alt', ogr.OFTReal))
layer.CreateField(ogr.FieldDefn('yaw', ogr.OFTReal))
layer.CreateField(ogr.FieldDefn('pitch', ogr.OFTReal))
layer.CreateField(ogr.FieldDefn('roll', ogr.OFTReal))
defn = layer.GetLayerDefn()
for i in np.arange(len(pointlist)):
	if ~np.isnan(float(pointlist[i,1])):
		if ~np.isnan(float(pointlist[i,1])):
			bearing_drift_corr = np.rad2deg(np.arctan2(np.sin(np.deg2rad(pointlist[i,8])+np.deg2rad(bearingdifflist[i])), np.cos(np.deg2rad(pointlist[i,8])+np.deg2rad(bearingdifflist[i]))))
			poly = EstimateFootprint(pointlist[i,7],float(pointlist[i,1]),float(pointlist[i,2]),float(pointlist[i,3]-pointlist[i,15]),float(bearing_drift_corr),float(pointlist[i,9]*-1+2),float(pointlist[i,10]*-1))
			feat = ogr.Feature(defn)
			feat.SetField('image', str(pointlist[i,7]))
			feat.SetField('dist',str(distlist[i]))
			feat.SetField('time',str(pointlist[i,6].time()))
			feat.SetField('lat',str(pointlist[i,1]))
			feat.SetField('lon',str(pointlist[i,2]))
			feat.SetField('alt',str(pointlist[i,3]))
			feat.SetField('yaw',str(pointlist[i,8]))
			feat.SetField('pitch',str(pointlist[i,9]))
			feat.SetField('roll',str(pointlist[i,10]))
			line.AddPoint(pointlist[i,4],pointlist[i,5])
			geom = poly
			feat.SetGeometry(geom)
			layer.CreateFeature(feat)
			feat = geom = None
			###rotate yaw by 180 degrees and change sign of pitch and roll
			bearing_drift_corr = np.rad2deg(np.arctan2(np.sin(np.deg2rad(bearing_drift_corr)+np.deg2rad(180)), np.cos(np.deg2rad(bearing_drift_corr)+np.deg2rad(180))))
			outfile.write(str(pointlist[i,7][:-4]+".jpg")+"; "+str(pointlist[i,1])+"; "+str(pointlist[i,2])+"; "+str(pointlist[i,3])+"; "+str(bearing_drift_corr)+"; "+str(pointlist[i,9]*-1+2)+"; "+str(pointlist[i,10]*-1)+"; "+str(pointlist[i,11])+"; "+str(pointlist[i,12])+"; "+str(pointlist[i,8])+"\n")
			if calc_drift_corr == 1:
				driftline = ogr.Geometry(ogr.wkbLineString)
				driftline.AddPoint(pointlist[i,4],pointlist[i,5])
				driftline.AddPoint(pointlist[i,13],pointlist[i,14])
				driftlines = np.append(driftlines,driftline)
outDataSource = layer = feat = geom = None
outfile.close()
os.system('cp '+JPGpath+'/Reference_data.txt '+outname_init+'_Reference_data.txt')

######create output line shapefile
dest_srs = ogr.osr.SpatialReference()
dest_srs.ImportFromEPSG(3413)
outShapefile = outname+'.shp'
outDriver = ogr.GetDriverByName('Esri Shapefile')
if os.path.exists(outShapefile):
	outDriver.DeleteDataSource(outShapefile)
outDataSource = outDriver.CreateDataSource(outShapefile)
layer = outDataSource.CreateLayer('', dest_srs, ogr.wkbLineString)
layer.CreateField(ogr.FieldDefn('flightID', ogr.OFTString))
defn = layer.GetLayerDefn()
feat = ogr.Feature(defn)
feat.SetField('flightID', outname_init)
feat.SetGeometry(line)
layer.CreateFeature(feat)
outDataSource = layer = feat = geom = None

######create output driftline shapefile
if calc_drift_corr == 1:
	dest_srs = ogr.osr.SpatialReference()
	dest_srs.ImportFromEPSG(3413)
	outShapefile = outname_init+'_driftpaths.shp'
	outDriver = ogr.GetDriverByName('Esri Shapefile')
	if os.path.exists(outShapefile):
		outDriver.DeleteDataSource(outShapefile)
	outDataSource = outDriver.CreateDataSource(outShapefile)
	layer = outDataSource.CreateLayer('', dest_srs, ogr.wkbLineString)
	layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger))
	layer.CreateField(ogr.FieldDefn('dist', ogr.OFTReal))
	defn = layer.GetLayerDefn()
	for i in np.arange(len(driftlines)):
		feat = ogr.Feature(defn)
		feat.SetField('id', str(i+1))
		geom = driftlines[i]
		feat.SetField('dist', geom.Length())
		feat.SetGeometry(geom)
		layer.CreateFeature(feat)
		feat = geom = None
	outDataSource = layer = feat = geom = None
	if os.path.exists(outname_init+'_driftpaths.zip'):
		os.system('rm '+outname_init+'_driftpaths.zip')
	os.system('zip '+outname_init+'_driftpaths.zip '+outname_init+'_driftpaths.dbf '+outname_init+'_driftpaths.prj '+outname_init+'_driftpaths.shp '+outname_init+'_driftpaths.shx')
	os.system('rm '+outname_init+'_driftpaths.dbf '+outname_init+'_driftpaths.prj '+outname_init+'_driftpaths.shp '+outname_init+'_driftpaths.shx')

####create geojson of survey area
ULlon,ULlat = np.nanmin(pointlist[:,2]),np.nanmax(pointlist[:,1])
URlon,URlat = np.nanmax(pointlist[:,2]),np.nanmax(pointlist[:,1])
LRlon,LRlat = np.nanmax(pointlist[:,2]),np.nanmin(pointlist[:,1])
LLlon,LLlat = np.nanmin(pointlist[:,2]),np.nanmin(pointlist[:,1])
geojson = open(outname+".geojson","w")
geojson.write('{\n')
geojson.write('"type": "FeatureCollection",\n')
geojson.write('"name": "'+outname+'",\n')
geojson.write('"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },\n')
geojson.write('"features": [\n')
geojson.write('{ "type": "Feature", "properties": { "id": null }, "geometry": { "type": "MultiPolygon", "coordinates": [ [ [ [ '+str(ULlon)+','+str(ULlat)+'], ['+str(URlon)+','+str(URlat)+'], ['+str(LRlon)+','+str(LRlat)+'], ['+str(LLlon)+', '+str(LLlat)+'], ['+str(ULlon)+', '+str(ULlat)+' ] ] ] ] } }\n')
geojson.write(']\n')
geojson.write('}\n')
geojson.close()

####create RoI shapefile around Polarstern
xmin = PSpsX_init-box_size/2.
xmax = PSpsX_init+box_size/2.
ymin = PSpsY_init-box_size/2.
ymax = PSpsY_init+box_size/2.

ULlon,ULlat,z = ct_back.TransformPoint(xmin,ymax)
URlon,URlat,z = ct_back.TransformPoint(xmax,ymax)
LRlon,LRlat,z = ct_back.TransformPoint(xmax,ymin)
LLlon,LLlat,z = ct_back.TransformPoint(xmin,ymin)

ring = ogr.Geometry(ogr.wkbLinearRing)	
ring.AddPoint(ULlon,ULlat)
ring.AddPoint(URlon,URlat)
ring.AddPoint(LRlon,LRlat)
ring.AddPoint(LLlon,LLlat)
ring.AddPoint(ULlon,ULlat)
poly = ogr.Geometry(ogr.wkbPolygon)
poly.AddGeometry(ring)

dest_srs = ogr.osr.SpatialReference()
dest_srs.ImportFromEPSG(4326)
outShapefile = outname+'_ROI_PS.shp'
outDriver = ogr.GetDriverByName('Esri Shapefile')
if os.path.exists(outShapefile):
	outDriver.DeleteDataSource(outShapefile)
outDataSource = outDriver.CreateDataSource(outShapefile)
layer = outDataSource.CreateLayer('', dest_srs, ogr.wkbPolygon)
defn = layer.GetLayerDefn()
feat = ogr.Feature(defn)
feat.SetGeometry(poly)
layer.CreateFeature(feat)
outDataSource = layer = feat = geom = None

#####Do the dishes
if os.path.exists(outname+'.zip'):
	os.system("rm "+outname+'.zip')
	os.system("rm "+outname+'_footprints.zip')
os.system('zip '+outname+'.zip '+outname+'.dbf '+outname+'.prj '+outname+'.shp '+outname+'.shx')
os.system('zip '+outname+'_footprints.zip '+outname+'_footprints.dbf '+outname+'_footprints.prj '+outname+'_footprints.shp '+outname+'_footprints.shx') 
os.system('zip '+outname+'_ROI_PS.zip '+outname+'_ROI_PS.dbf '+outname+'_ROI_PS.prj '+outname+'_ROI_PS.shp '+outname+'_ROI_PS.shx')
os.system('rm '+outname+'.dbf '+outname+'.prj '+outname+'.shp '+outname+'.shx')
os.system('rm '+outname+'_footprints.dbf '+outname+'_footprints.prj '+outname+'_footprints.shp '+outname+'_footprints.shx')
os.system('rm '+outname+'_ROI_PS.dbf '+outname+'_ROI_PS.prj '+outname+'_ROI_PS.shp '+outname+'_ROI_PS.shx')

