#!/usr/bin/env python2.7
import sys
import os
import open3d as o3d
import laspy
import numpy as np

#script to filter agisoft dense point clouds via open3d filtering routines. N. Neckel 2022

ProjectName = sys.argv[1]
neighbors = int(sys.argv[2])
std_ratio = float(sys.argv[3])

#Load agisoft las file including surface elevations and confidence
#las = laspy.read(ProjectName+".las") # laspy 2.0
las = laspy.file.File(ProjectName+".las", mode ="r") # laspy 1.7
confidence = las.confidence
X = las.X*las.header.scale[0]+las.header.offset[0]
Y = las.Y*las.header.scale[1]+las.header.offset[1]
Z = las.Z*las.header.scale[2]+las.header.offset[2]

#Convert to open3d point cloud
pcd = o3d.geometry.PointCloud()
points = np.c_[X,Y,Z]
pcd.points = o3d.utility.Vector3dVector(points)

#Calculate average distance between points
distances = pcd.compute_nearest_neighbor_distance()
avg_dist = np.mean(distances)
print("average point distance is: "+str(round(avg_dist,2))+" m.")

#1. filter points which deviate by more than 50 m (height of Polarstern) to a fitted plane
plane_model, inliers = pcd.segment_plane(distance_threshold=50,ransac_n=5,num_iterations=1000)
#flt_pcd = pcd.select_by_index(inliers) #python3
flt_pcd = pcd.select_down_sample(inliers)
flt_confidence = confidence[inliers] #apply index to confidence point cloud

#2. statistical filter on the average distance of a moving window
flt_pcd, ind = flt_pcd.remove_statistical_outlier(nb_neighbors=neighbors,std_ratio=std_ratio)
o3d.io.write_point_cloud(ProjectName+"_flt.pcd", flt_pcd) #export points to pcd file
flt_pcd = np.asarray(flt_pcd.points) #convert open3d point cloud to numpy array
flt_confidence = flt_confidence[ind]
np.savetxt('elevation.txt', np.c_[flt_pcd[:,0],flt_pcd[:,1],flt_pcd[:,2],flt_confidence]) #export points to txt file for gridding with ames point2dem

#gridding of DEM and confidence via ames point2dem tool
os.system("point2dem --csv-format '1:easting 2:northing 4:height_above_datum' --t_srs '+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs' --tr 0.5 elevation.txt -o tmp --threads 18 --rounding-error 1")
os.system("mv tmp-DEM.tif "+ProjectName+"_confidence_0.5m.tif")
os.system("point2dem --csv-format '1:easting 2:northing 3:height_above_datum' --t_srs '+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs' --tr 0.5 elevation.txt -o tmp --threads 18")
os.system("mv tmp-DEM.tif "+ProjectName+"_DEM_0.5m.tif")
os.system("rm tmp* elevation.txt")














