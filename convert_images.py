#!/usr/bin/env python2.7

import numpy as np
import fnmatch
import sys
import os
import datetime as dt
from tqdm import tqdm
from PIL import Image
import time

###Convert MOSAiC camera images from RAW format to JPG format employing vignetting correction to use with agisoft metashape.
###N. Fuchs and N. Neckel

def GetCAMList(path,wildcard):
	filelist = []
	for file in os.listdir(path):
		if fnmatch.fnmatch(file, wildcard):
			if len(file.split(",")) == 2:
				filename = file.split(",")[0]+"_"+file.split(",")[1]
				filelist = np.append(filelist,filename)
				os.system("mv "+path+"/"+file+" "+path+"/"+filename)
			else:
				filelist = np.append(filelist,file)
	return np.sort(filelist)

def FindStringBetweenSign(file,sign):
        value = file.split(sign)
        return value

opt_mean=np.load('Vignette_correction_image_opt_512138_FLT6921_14mm_FLT1889_2000701_mosaics.npy')
bvalue_init = 1

mainpath = sys.argv[1]
RAWpath = mainpath+"/RAW"
CAMlist = GetCAMList(RAWpath,"*CR2")
ISOlist = []
LVlist = []

os.chdir(RAWpath)
print("scan images for different ISO values:")
for i in tqdm(range(len(CAMlist))):
	tags = os.popen("exiftool -n -f "+CAMlist[i]+" -Iso -LightValue").readlines()
	ISO = tags[0].split()[2]
	ISOlist = np.append(ISOlist,float(ISO))
	LV = tags[1].split()[3]
	LVlist = np.append(LVlist,float(LV))
maxISO = np.max(ISOlist)
minLV = np.min(LVlist)

if len(np.unique(LVlist)) > 1:
	os.system("mkdir "+mainpath+"/JPG_bcorr")
	
print("adjust brightness, match ISO values and apply vignette:")
for i in tqdm(range(len(CAMlist))):
	bvalue = bvalue_init
	os.system("dcraw -T -t 0 -W -b "+str(bvalue)+" -r 2.036133 1.000000 1.471680 1.0000000 -C 0.99950424 0.999489075 -n 40 "+CAMlist[i])
	####apply vignette
	opt = np.float32(Image.open(CAMlist[i][:-4]+".tiff"))
	for t in range(3):
		opt[:,:,t] = np.clip(opt[:,:,t] * opt_mean[:,:,t],0,255)
    	Image.fromarray(np.uint8(opt)).save(CAMlist[i][:-4]+"_dev.tiff")
	os.system("convert -quality 100 "+CAMlist[i][:-4]+"_dev.tiff "+CAMlist[i][:-4]+".jpg")
    	os.system("exiftool -TagsfromFile "+CAMlist[i]+" "+CAMlist[i][:-4]+".jpg -overwrite_original")
	time.sleep(1)
	os.system("mv "+CAMlist[i][:-4]+".jpg "+mainpath+"/JPG")
	os.system("rm "+CAMlist[i][:-4]+".tiff "+CAMlist[i][:-4]+"_dev.tiff")
	if os.path.exists(mainpath+"/JPG_bcorr"):
		os.system("cp "+mainpath+"/JPG/"+CAMlist[i][:-4]+".jpg "+mainpath+"/JPG_bcorr")	
	tags = os.popen("exiftool -n -f "+CAMlist[i]+" -LightValue").readlines()
	LV = float(tags[0].split()[3])
	bvalue = bvalue*2**(LV-minLV)
	if bvalue != bvalue_init:
		os.system("dcraw -T -t 0 -W -b "+str(bvalue)+" -r 2.036133 1.000000 1.471680 1.0000000 -C 0.99950424 0.999489075 -n 40 "+CAMlist[i])
		####apply vignette
		opt = np.float32(Image.open(CAMlist[i][:-4]+".tiff"))
		for t in range(3):
			opt[:,:,t] = np.clip(opt[:,:,t] * opt_mean[:,:,t],0,255)
    		Image.fromarray(np.uint8(opt)).save(CAMlist[i][:-4]+"_dev.tiff")
		os.system("convert -quality 100 "+CAMlist[i][:-4]+"_dev.tiff "+CAMlist[i][:-4]+".jpg")
    		os.system("exiftool -TagsfromFile "+CAMlist[i]+" "+CAMlist[i][:-4]+".jpg -overwrite_original")
		time.sleep(1)
		os.system("mv "+CAMlist[i][:-4]+".jpg "+mainpath+"/JPG_bcorr")
		os.system("rm "+CAMlist[i][:-4]+".tiff "+CAMlist[i][:-4]+"_dev.tiff")
