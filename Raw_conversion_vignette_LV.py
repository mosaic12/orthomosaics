# -*- coding: utf-8 -*-
#!~/anaconda3/envs/myenv/bin/python3.7
import numpy as np
import cv2
import sys
import os
import fnmatch

###Script to convert MOSAIC RAW camera data to ppm format including brightness adjustment if changing camera settings are detected.
###N. Fuchs and N. Neckel 2022

def read_ppm(file,size):
	infile = open(file,'rb')
	
	type = infile.readline().decode('UTF-8').rstrip()
	dim = infile.readline().decode('UTF-8').rstrip().split(' ')
	width = int(dim[0])
	height = int(dim[1])
	maxval = int(infile.readline().decode('UTF-8').rstrip().split('.')[0])
	if size=='16bit':
		image = np.fromfile(infile, dtype='>u2').reshape((height, width,3))
	elif size=='8bit':
		image = np.fromfile(infile, dtype='>u1').reshape((height, width,3))
	return image
	
def write_ppm(data,file,size):
	with open(file,'w') as outfile:
		outfile.write('P6\n')
		outfile.write(str(data.shape[1])+' '+str(data.shape[0])+'\n')
		maxval = int(np.nanmax(data))
		outfile.write(str(int(maxval))+'\n')
		if size=='8bit':
			data=np.uint8(data)
			data.astype('>u1').tofile(outfile)
		elif size == '16bit':
			data=np.uint16(data)
			data.astype('>u2').tofile(outfile)

def GetFileList(path,wildcard):
	filelist = []
	for file in os.listdir(path):
		if fnmatch.fnmatch(file, wildcard):
			filelist = np.append(filelist,file)
	return np.sort(filelist)

def FindStringBetweenSign(file,sign):
	value = file.split(sign)
	return value

linear_opt_vign_file = 'Vignette_correction_image_linear_opt_512138_FLT6921_14mm_FLT1889_200601.npy'
linear_opt_vign = np.load(linear_opt_vign_file)
bvalue_init = 1

RAWpath = sys.argv[1]
CAMlist = GetFileList(RAWpath,"*.CR2")
LVlist = []

os.chdir(RAWpath)
print("scan images for different LV values:")
for i in np.arange(len(CAMlist)):
	tags = os.popen("exiftool -n -f "+CAMlist[i]+" -LightValue").readlines()
	LV = tags[0].split()[3]
	LVlist = np.append(LVlist,float(LV))
minLV = np.min(LVlist)

for i in np.arange(len(CAMlist)):
	pic = CAMlist[i]
	if fnmatch.fnmatch(pic,'*.CR2') and os.path.getsize(pic) != 0:
		if not os.path.isfile(pic.rsplit('.',1)[0]+'.ppm'):
			bvalue = bvalue_init
			tags = os.popen("exiftool -n -f "+CAMlist[i]+" -LightValue").readlines()
			LV = float(tags[0].split()[3])
			bvalue = bvalue*2**(LV-minLV)
			print(bvalue)
			os.system('dcraw -t 0 -o 0 -k 1025 -S 15280 -r 1 1 1 1 -W -b '+str(bvalue)+' -g 1 1 -6 -j -C 0.99950424 0.999489075 -n 100 ' + pic)
			if os.path.isfile(pic.rsplit('.',1)[0]+'.ppm'):
				linear_opt_image = np.float32(read_ppm(pic.rsplit('.',1)[0]+'.ppm','16bit'))
				if 'vign_corr' not in locals():
					vign_corr=cv2.resize(linear_opt_vign,dsize=(linear_opt_image.shape[1],linear_opt_image.shape[0]),interpolation=cv2.INTER_LINEAR)
				write_ppm(np.clip(linear_opt_image*vign_corr,0,(2**16)-1),pic.rsplit('.',1)[0]+'.ppm','16bit')
