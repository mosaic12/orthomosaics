import Metashape
import os
import glob
import math
import sys
import laspy
import numpy as np
import zipfile
import gdal

###AGISOFT MASTER SCRIPT BY N.NECKEL 2022

ProjectDirectory = sys.argv[1]
ProjectName = ProjectDirectory.split("/")[-1]

####Settings
keypoint_limit=int(sys.argv[2])
tiepoint_limit=int(sys.argv[3])
neighbors=1000
std_ratio=1
quantile_filter=int(sys.argv[4])

doc = Metashape.Document()
doc.save(path = ProjectDirectory+"/"+ProjectName+".psx") #new empty project is created

# DEFINE PROCESSING SETTINGS
print("---Defining processing settings...")

# Define: Camera calib file
CalibFile = "Camera_calib_initial_values_14mm_MOSAiC.xml"

# Define: Source images
AerialImagesPattern = "./JPG/*.jpg"

# Define: Orientation file
OrientationFile = "./JPG/Reference_data.txt"

# Set home folder
os.chdir(ProjectDirectory)
print("Home directory: " + ProjectDirectory )

# create chunk
chunk = doc.addChunk()
chunk.label = ProjectName

# FIND ALL PHOTOS IN PATH
Images = glob.glob(AerialImagesPattern)
chunk.addPhotos(Images)
doc.save()

def CameraError(cameras):
	X = [] 
	Y = []
	Z = []
	for camera in cameras:
		if not camera.transform:
			continue
		if not camera.reference.location:
			continue
		estimated_geoc = chunk.transform.matrix.mulp(camera.center)
		error = chunk.crs.unproject(camera.reference.location) - estimated_geoc
		X = np.append(X,error[0]**2)
		Y = np.append(Y,error[1]**2)
		Z = np.append(Z,error[2]**2)
	X_error = np.sqrt(np.sum(X)/len(X))
	Y_error = np.sqrt(np.sum(Y)/len(Y))
	Z_error = np.sqrt(np.sum(Z)/len(Z))
	XY_error=np.sqrt(np.sum(X+Y)/len(X))
	total_error=np.sqrt(np.sum(X+Y+Z)/len(X))
	return X_error,Y_error,Z_error,XY_error,total_error

# INITIAL ALIGNEMT AND CAMERA OPTIMIZATION
crs = Metashape.CoordinateSystem("EPSG::4326")
chunk.importReference(path=OrientationFile, format=Metashape.ReferenceFormatCSV, columns='nyxzabc', crs = crs, delimiter=';', create_markers=False,skip_rows=1)
user_calib = Metashape.Calibration()
user_calib.load(CalibFile)
for sensor in chunk.sensors:
	sensor.user_calib = user_calib
	sensor.fixed = False
for camera in chunk.cameras:
	camera.reference.rotation_enabled = True
doc.save()

chunk.matchPhotos(reference_preselection=True,keypoint_limit=keypoint_limit,tiepoint_limit=tiepoint_limit)
chunk.alignCameras(adaptive_fitting=True)
chunk.optimizeCameras(adaptive_fitting=True,tiepoint_covariance=True)
doc.save()

# FILTER TIEPOINTS BY 95% ERROR QUANTILE
if quantile_filter == 1:
	error_list = []
	for camera in chunk.cameras:
		if not camera.transform:
			continue
		if not camera.reference.location:
			continue
		estimated_geoc = chunk.transform.matrix.mulp(camera.center)
		error = chunk.crs.unproject(camera.reference.location) - estimated_geoc
		error = error.norm()
		error_list.append(error)
	threshold = np.quantile(error_list,0.95)

	point_cloud = chunk.point_cloud
	points = point_cloud.points
	projections = point_cloud.projections
	point_ids = [-1] * len(point_cloud.tracks)

	for point_id in range(0, len(points)):
		point_ids[points[point_id].track_id] = point_id

	crs = chunk.crs
	for camera in chunk.cameras:
		if not camera.transform:
			continue
		if not camera.reference.location:
			continue

		estimated_geoc = chunk.transform.matrix.mulp(camera.center)
		error = chunk.crs.unproject(camera.reference.location) - estimated_geoc
		error = error.norm()
		if error > threshold:
			for proj in projections[camera]:
				track_id = proj.track_id
				point_id = point_ids[track_id]
				if point_id < 0:
					continue
				if not points[point_id].valid:
					continue
				points[point_id].valid = False
	chunk.optimizeCameras(adaptive_fitting=True,tiepoint_covariance=True)

# FILTER TIEPOINTS BY RECONSTRUCTION UNCERTAINTY AND CAMERA OPTIMIZATION
f = Metashape.PointCloud.Filter()
f.init(chunk,Metashape.PointCloud.Filter.ReconstructionUncertainty)
f.removePoints(10)
chunk.optimizeCameras(adaptive_fitting=True,tiepoint_covariance=True)
doc.save()

errorfile = open(ProjectName+'_errors.txt','w')
X_error,Y_error,Z_error,XY_error,total_error = CameraError(chunk.cameras)
errorfile.write(str(X_error)+' '+str(Y_error)+' '+str(Z_error)+' '+str(XY_error)+' '+str(total_error)+'\n')
errorfile.close()

# FILTER DEPTHMAP AND DENSE CLOUD
chunk.buildDepthMaps(filter_mode=Metashape.AggressiveFiltering)
doc.save()
chunk.buildDenseCloud(point_confidence=True)
doc.save()

# EXPORT DENSE CLOUD REFERENCED TO DTU MSS
d_projection = Metashape.OrthoProjection()
d_projection.crs.addGeoid(path="/home/ollie/nneckel/metashape-pro/geoids/DTU21MSS_1min.mss2.tif")
d_projection.crs = Metashape.CoordinateSystem('COMPD_CS["EPSG3413_MSS",PROJCS["WGS 84 / NSIDC Sea Ice Polar Stereographic North",GEOGCS["WGS 84",DATUM["World Geodetic System 1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9102"]],AUTHORITY["EPSG","4326"]],PROJECTION["PolarStereographicB",AUTHORITY["EPSG","9829"]],PARAMETER["latitude_of_origin",70],PARAMETER["central_meridian",-45],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","3413"]],VERT_CS["DTUMSS",VERT_DATUM["MSS_DTU",2005],UNIT["metre",1,AUTHORITY["EPSG","9001"]]]]')
compression = Metashape.ImageCompression()
compression.tiff_big = True
chunk.exportPoints(path=ProjectDirectory+"/"+ProjectName+".las",source_data=Metashape.DenseCloudData,save_colors=False,save_classes=False,save_normals=False,save_confidence=True,crs=d_projection.crs)

# FILTER DENSE CLOUD BY OPEN3D PLANE FILTER AND STATISTICAL FILTER
# BUILD DEM AND CONFIDENCE GRID via AMES STEREOPIPLINE
os.system("filter_point_cloud.py "+ProjectName+" "+str(neighbors)+" "+str(std_ratio))

chunk.importPoints(path=ProjectName+"_flt.pcd",crs=d_projection.crs)

# DEFINE REGION FOR FINAL DATASET
src = gdal.Open(ProjectName+"_confidence_0.5m.tif")
ulx, xres, xskew, uly, yskew, yres  = src.GetGeoTransform()
lrx = ulx + (src.RasterXSize * xres)
lry = uly + (src.RasterYSize * yres)

region = Metashape.BBox()
region.min = Metashape.Vector([ulx,lry])
region.max = Metashape.Vector([lrx,uly])
#####

# BUILD AND EXPORT INTERPOLATED DEM AND ORTHOIMAGES via AGISOFT AT 0.5 M RESOLUTION AND FULL RESOLUTION AROUND POLARSTERN 
chunk.buildDem(source_data=Metashape.DenseCloudData,projection=d_projection)
chunk.buildOrthomosaic(surface_data=Metashape.ElevationData,fill_holes=False,projection=d_projection)
doc.save()
chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_DEM_int_0.5m.tif",projection=d_projection,source_data=Metashape.ElevationData,resolution_x=0.5,resolution_y=0.5,image_compression = compression, region=region)
chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_orthophoto_0.5m.tif",projection=d_projection,source_data=Metashape.OrthomosaicData,resolution_x=0.5,resolution_y=0.5,image_compression = compression, region=region)
chunk.exportReport(path=ProjectDirectory+"/"+ProjectName+"_report.pdf", title=ProjectName)
doc.save()
for name in glob.glob('*ROI_PS.zip'):
	zip = zipfile.ZipFile(name)
	zip.extractall()
	shpfile = name[:-4]+".shp"
chunk.importShapes(path = ProjectDirectory+'/'+shpfile, boundary_type = Metashape.Shape.OuterBoundary, crs = Metashape.CoordinateSystem('EPSG::4326'))
os.system("rm "+shpfile+" "+shpfile[:-4]+".dbf "+shpfile[:-4]+".shx "+shpfile[:-4]+".prj")
chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_orthophoto_PS_crop.tif",projection=d_projection,source_data=Metashape.OrthomosaicData,clip_to_boundary=True,image_compression = compression)
chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_DEM_int_PS_crop.tif",projection=d_projection,source_data=Metashape.ElevationData,clip_to_boundary=True,image_compression = compression)

for camera in chunk.cameras:
	camera.photo.path = "/".join(["RAW/Lin_corr", camera.photo.path.rsplit("/",1)[1]])
chunk.buildOrthomosaic(surface_data=Metashape.ElevationData,projection=d_projection)
chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_orthophoto_lin_corr_0.5m.tif",projection=d_projection,source_data=Metashape.OrthomosaicData,clip_to_boundary=False,resolution_x=0.5,resolution_y=0.5,image_compression = compression, region=region)
chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_orthophoto_lin_corr_PS_crop.tif",projection=d_projection,source_data=Metashape.OrthomosaicData,clip_to_boundary=True,image_compression = compression)
doc.save()
if os.path.exists(ProjectDirectory+"/JPG_bcorr"):
	for camera in chunk.cameras:
		camera.photo.path = "/".join(["JPG_bcorr", camera.photo.path.rsplit("/",1)[1]])
	chunk.buildOrthomosaic(surface_data=Metashape.ElevationData,projection=d_projection)
	chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_orthophoto_bcorr_0.5m.tif",projection=d_projection,source_data=Metashape.OrthomosaicData,clip_to_boundary=False,resolution_x=0.5,resolution_y=0.5,image_compression = compression, region=region)
	chunk.exportRaster(path=ProjectDirectory+"/"+ProjectName+"_orthophoto_bcorr_PS_crop.tif",projection=d_projection,source_data=Metashape.OrthomosaicData,clip_to_boundary=True,image_compression = compression)
doc.save()
Metashape.Document()
