#!/usr/bin/env python3

import numpy as np
from import_export_geotiff import resampleGeoTiff,ExportGeoTiff
from astropy.convolution import convolve, convolve_fft
from astropy.convolution import Gaussian2DKernel
import os
from skimage import morphology
from osgeo import gdal, gdalconst
from subprocess import Popen, PIPE
import pylab as py
import sys

###Script to compensate orthomosaics for cloudshadows via ALS reflectance map.
###N. Neckel 2022

infile = sys.argv[1]
RFL = sys.argv[2]
scalelimit = 0.65
islandsize = 1000000

def RemCldShd(bNo):
	cmd = 'gdal_translate {} -b {} band.tif'.format(infile,bNo)
	p = Popen(cmd, shell=True)
	p.wait()
	cmd = 'gdalwarp band.tif -tr 10 10 -r bilinear band_10m.tif -overwrite'
	p = Popen(cmd, shell=True)
	p.wait()

	band,refl,width,height,match_geotrans,match_proj = resampleGeoTiff("band_10m.tif",RFL)
	band,alpha,width,height,match_geotrans,match_proj = resampleGeoTiff("band_10m.tif","alpha.tif")

	###get rid of no-data values
	band = np.where(alpha==0,np.nan,band)
	refl = np.where(refl==0,np.nan,refl)

	###rescale reflectance to 0-255
	refl = refl*-1
	maxval=np.nanmax(refl)
	refl=(np.float32(refl)/float(maxval))*255.
	refl=abs(refl-255)

	###smooth reflectance
	kernel = Gaussian2DKernel(x_stddev=3)
	refl_flt = convolve(refl, kernel)
	band_flt = convolve(band, kernel)

	###calculate ratio for scaling	
	scale = refl_flt/band_flt
	scale = np.where(np.isinf(scale),np.nan,scale)
	
	scale = np.where(scale<scalelimit,scalelimit,scale)

	ExportGeoTiff("scale.tif",scale,width,height,match_geotrans,match_proj)

	band,scale,width,height,match_geotrans,match_proj = resampleGeoTiff("band.tif","scale.tif")
	band,alpha,width,height,match_geotrans,match_proj = resampleGeoTiff("band.tif","alpha.tif")

	###scale band
	corr = band*scale
	corr = np.where((alpha==0)|(scale==0),np.nan,corr)
	maxval=np.nanmax(corr)
		
	###Build mask to remove small objects
	binarized = np.where(np.isnan(corr), 0, 1)
	mask = morphology.remove_small_objects(binarized.astype(bool), min_size=islandsize, connectivity=0)
	corr = np.where(mask==1,corr,0)
	outdriver = gdal.GetDriverByName("GTiff")
	outdata   = outdriver.Create("b"+str(bNo)+"_corr.tif", width, height, 1, gdalconst.GDT_Byte)
	outdata.GetRasterBand(1).WriteArray(corr)
	outdata.SetGeoTransform(match_geotrans)
	outdata.SetProjection(match_proj)

	if bNo == 1:
		alpha = np.where((alpha==0)|(scale==0),0,255)
		alpha = np.where(mask==1,alpha,0)
		outdriver = gdal.GetDriverByName("GTiff")
		outdata   = outdriver.Create("alpha_corr.tif", width, height, 1, gdalconst.GDT_Byte)
		outdata.GetRasterBand(1).WriteArray(alpha)
		outdata.SetGeoTransform(match_geotrans)
		outdata.SetProjection(match_proj)
	
	os.system('rm band* scale.tif')
	
	return corr

cmd = 'gdal_translate {} -b 4 alpha.tif'.format(infile)
p = Popen(cmd, shell=True)
p.wait()
for i in np.arange(3):
	RemCldShd(i+1)
if os.path.exists(infile[:-4]+'b.tif'):
	os.system('rm '+infile[:-4]+'b.tif')
cmd = 'gdal_merge.py b1_corr.tif b2_corr.tif b3_corr.tif alpha_corr.tif -o {} -separate -co COMPRESS=DEFLATE -co TILED=YES'.format(infile[:-4]+'b.tif')
p = Popen(cmd, shell=True)
p.wait()

cmd = 'gdal_edit.py {} -mo "AUTHOR=N. Neckel, AWI"'.format(infile[:-4]+'b.tif')
p = Popen(cmd, shell=True)
p.wait()
os.system('rm b**corr.tif alpha_corr.tif alpha.tif')

