#!/usr/bin/env python

import xarray as xr
import numpy as np
from osgeo import ogr, osr, gdal, gdalconst
import os
from subprocess import Popen, PIPE
from import_export_geotiff import ImportGeoTiff, ExportGeoTiff
from affine import Affine
import sys
import pylab as py
import zipfile
from glob import glob

###Script to match DEMs and orthomosaics via NASA's AMES stereo pipeline
###N. Neckel 2022

flightID = sys.argv[1] 
ncfile = sys.argv[2] ###input netCDF file
rotation = float(sys.argv[3]) ###rotation in degrees
flightName = flightID+'_'+sys.argv[4]
surface_sampling = float(sys.argv[5]) ###interpolation method for point2dem when scaling and rotating full resolution results, can be 1 for yes or 0 for no

if (len(sys.argv) > 6):
	ROIZIP = sys.argv[6] ###zipped shapefile for matching area, if not provided entire grid is used

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def RescaleBand(bandname,outname):
	data,width,height,match_geotrans,match_proj = ImportGeoTiff(bandname)
	maxval=np.nanmax(data)
	if maxval > 100 and np.nanmedian(data) < 1:
		scaled_band=data*255.
	else:
		scaled_band=(np.float32(data)/float(maxval))*255.
	outdriver = gdal.GetDriverByName("GTiff")
	outdata   = outdriver.Create(outname, width, height, 1, gdalconst.GDT_Byte, options=['BIGTIFF=YES'])
	outdata.GetRasterBand(1).WriteArray(scaled_band)
	outdata.SetGeoTransform(match_geotrans)
	outdata.SetProjection(match_proj)

def rotate_gt(affine_matrix, angle, pivot=None):
	affine_src = Affine.from_gdal(*affine_matrix)
	affine_dst = affine_src * affine_src.rotation(angle, pivot)
	return affine_dst.to_gdal()

def detrendDEM(ALS_DEM,CANON_DEM):
	cmd = 'r.in.gdal input={} output=CANON_DEM --overwrite'.format(CANON_DEM)
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.in.gdal input={} output=ALS_DEM --overwrite'.format(ALS_DEM)
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'g.region raster=CANON_DEM res=5'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.mask raster=CANON_DEM'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = "r.mapcalc 'diff = CANON_DEM-ALS_DEM' --overwrite"
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.neighbors input=diff output=diff_int size=51 method=average --overwrite'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.mask -r'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'g.region raster=CANON_DEM res=0.5'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.resamp.interp input=diff_int output=diff_int_fine method=bilinear'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.mask raster=CANON_DEM'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = "r.mapcalc 'CANON_DEM_corr = CANON_DEM-diff_int_fine' --overwrite"
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.mask -r'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'r.out.gdal input=CANON_DEM_corr output={} createopt="COMPRESS=LZW,TILED=YES" -cm --overwrite'.format(flightName+'_DEM_hr.tif')
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'g.remove type=raster name=CANON_DEM,ALS_DEM,diff,diff_int,diff_int_fine,CANON_DEM_corr -f'
	p = Popen(cmd, shell=True)
	p.wait()

	cmd = 'gdal_edit.py {} -mo "AUTHOR=N. Neckel, AWI"'.format(flightName+'_DEM_hr.tif')
	p = Popen(cmd, shell=True)
	p.wait()

netCDF = xr.open_mfdataset(ncfile)
elev = netCDF['elevation'].values
if np.nanmin(elev) > 15:
	elev_reference = netCDF['elevation_reference'].values
	elev = elev-elev_reference
refl = netCDF['reflectance'].values
x = netCDF['xc'].values
y = netCDF['yc'].values
lat = netCDF['lat'].values
lon = netCDF['lon'].values

xidx = find_nearest(x,0)
yidx = find_nearest(y,0)

center_lon = lon[yidx,xidx]
center_lat = lat[yidx,xidx]

####Transform coordinates between lat lon and polar stereo (ps)
srs_in = osr.SpatialReference()
srs_in.ImportFromEPSG(4326)
srs_out = osr.SpatialReference()
srs_out.ImportFromEPSG(3413)
ct = osr.CoordinateTransformation(srs_in,srs_out)
center_x,center_y,center_z = ct.TransformPoint(center_lon,center_lat)

srs = '+proj=stere +lat_0='+str(center_lat)+' +lon_0='+str(center_lon)+' +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
srs_out = osr.SpatialReference()
srs_out.ImportFromProj4(srs)

########Export to Geotif
nx,ny = elev.shape[1],elev.shape[0]
xres = 0.5
yres = -0.5
geotransform = (x.min()-0.25, xres, 0, y.max()+0.25, 0, yres)
rot_geotrans = rotate_gt(geotransform, rotation, (xidx,ny-yidx))

ExportGeoTiff("tmp.tif",np.flipud(elev),nx,ny,tuple(rot_geotrans),srs_out.ExportToWkt())
cmd = 'gdalwarp tmp.tif -t_srs EPSG:3413 -tr 0.5 0.5 -co TILED=YES -co compress=lzw -r bilinear {} -overwrite'.format(flightID+'_ALS_DEM_0.5m.tif')
p = Popen(cmd, shell=True)
p.wait()
ExportGeoTiff("tmp.tif",np.flipud(refl),nx,ny,tuple(rot_geotrans),srs_out.ExportToWkt())
cmd = 'gdalwarp tmp.tif -t_srs EPSG:3413 -tr 0.5 0.5 -co TILED=YES -co compress=lzw -r bilinear {} -overwrite'.format(flightID+'_ALS_reflectance_0.5m.tif')
p = Popen(cmd, shell=True)
p.wait()

########Align photogrammetric DEM and orthomosaic with ALS DEM using AMES Stereopipeline
if 'ROIZIP' in locals():
	zip = zipfile.ZipFile(ROIZIP)
	zip.extractall()
	cmd = 'gdalwarp {} -cutline {} -t_srs EPSG:3413 -crop_to_cutline {} -overwrite'.format(flightID+'_ALS_DEM_0.5m.tif',ROIZIP[:-4]+'.shp',flightID+'_ALS_DEM_0.5m_crop.tif')
	p = Popen(cmd, shell=True)
	p.wait()
	cmd = 'pc_align --max-displacement 100 {} {} -o tmp --save-transformed-source-points --initial-transform-from-hillshading similarity --threads 18'.format(flightID+'_ALS_DEM_0.5m_crop.tif',flightID+'_DEM_0.5m.tif')
	p = Popen(cmd, shell=True)
	p.wait()
	os.system('rm '+ROIZIP[:-4]+'.shp '+ROIZIP[:-4]+'.dbf '+ROIZIP[:-4]+'.prj '+ROIZIP[:-4]+'.shx '+flightID+'_ALS_DEM_0.5m_crop.tif')
else:
	cmd = 'pc_align --max-displacement 100 {} {} -o tmp --save-transformed-source-points --initial-transform-from-hillshading similarity --threads 18'.format(flightID+'_ALS_DEM_0.5m.tif',flightID+'_DEM_0.5m.tif')
	p = Popen(cmd, shell=True)
	p.wait()

####Generate shifted confidence and DEM grid

cmd = 'point2dem tmp-trans_source.tif {} --orthoimage --nodata-value nan --tr 0.5 --threads 18'.format(flightID+'_confidence_0.5m.tif')
p = Popen(cmd, shell=True)
p.wait()

cmd = 'gdalwarp tmp-trans_source-DRG.tif -ot Byte -co TILED=YES -co COMPRESS=LZW {} -overwrite -dstnodata 0'.format(flightName+'_confidence_hr.tif')
p = Popen(cmd, shell=True)
p.wait()

cmd = 'gdal_edit.py {} -mo "AUTHOR=N. Neckel, AWI"'.format(flightName+'_confidence_hr.tif')
p = Popen(cmd, shell=True)
p.wait()

os.system('mv tmp-trans_source-DEM.tif '+flightID+'_DEM_0.5m_ALS_aligned.tif')
os.system('mv tmp-transform.txt '+flightID+'_ALS_transform.txt')
log_file = glob('tmp-log-pc_align**txt')[0]
os.rename(log_file,flightID+'_ALS_pc_align_log.txt')
detrendDEM(flightID+'_ALS_DEM_0.5m.tif',flightID+'_DEM_0.5m_ALS_aligned.tif')

########Align high resolution orthoimage to ALS
for i in range(4):
	cmd = 'gdal_translate {} -b {} {} -co TILED=YES -co compress=lzw'.format(flightID+'_orthophoto_lin_corr_0.5m.tif',i+1,'b'+str(i+1)+'.tif')
	p = Popen(cmd, shell=True)
	p.wait()

	if i == 0:
		####Extract center Geotiff
		cmd = 'gdalwarp {} {} -te {} -overwrite'.format('b'+str(i+1)+'.tif','b1_center.tif',str(center_x-5)+' '+str(center_y-5)+' '+str(center_x+5)+' '+str(center_y+5))
		p = Popen(cmd, shell=True)
		p.wait()
	
		####Algin croped Geotiffs
		cmd = 'pc_align --max-displacement -1 --num-iterations 0 --save-transformed-source-points --initial-transform {} {} {} -o tmp --threads 18'.format(flightID+'_ALS_transform.txt','b1_center.tif','b'+str(i+1)+'.tif')
		p = Popen(cmd, shell=True)
		p.wait()

	cmd = 'point2dem tmp-trans_source.tif {} --orthoimage --nodata-value nan --tr 0.5 --threads 18'.format('b'+str(i+1)+'.tif')
	p = Popen(cmd, shell=True)
	p.wait()

	RescaleBand('tmp-trans_source-DRG.tif','b'+str(i+1)+'_shift.tif')
	os.remove('b'+str(i+1)+'.tif')

if os.path.exists(flightName+'_orthomosaic_hr_l2.tif'):
	os.remove(flightName+'_orthomosaic_hr_l2.tif')

cmd = 'gdal_merge.py {} {} {} {} -separate -co COMPRESS=DEFLATE -co BIGTIFF=YES -co TILED=YES -o {}'.format('b1_shift.tif','b2_shift.tif','b3_shift.tif','b4_shift.tif',flightName+'_orthomosaic_hr_l2.tif')
p = Popen(cmd, shell=True)
p.wait()
cmd = 'gdal_edit.py {} -mo "AUTHOR=N. Neckel, AWI"'.format(flightName+'_orthomosaic_hr_l2.tif')
p = Popen(cmd, shell=True)
p.wait()
os.system('rm tmp* b1_shift.tif b2_shift.tif b3_shift.tif b4_shift.tif b1_center.tif')

########Align full resolution orthoimage to ALS
for i in range(4):
	cmd = 'gdal_translate {} -b {} {} -co TILED=YES -co compress=lzw'.format(flightID+'_orthophoto_lin_corr_PS_crop.tif',i+1,'b'+str(i+1)+'.tif')
	p = Popen(cmd, shell=True)
	p.wait()

	if os.path.exists(flightID+'_highres_roi.shp'):
		cmd = 'gdalwarp {} {} -cutline {} -crop_to_cutline -overwrite'.format('b'+str(i+1)+'.tif','out.tif',flightID+'_highres_roi.shp')
		p = Popen(cmd, shell=True)
		p.wait()
		os.system('mv out.tif b'+str(i+1)+'.tif')

	if i == 0:
		####Extract center Geotiff
		cmd = 'gdalwarp {} {} -te {} -overwrite'.format('b'+str(i+1)+'.tif','b1_center.tif',str(center_x-5)+' '+str(center_y-5)+' '+str(center_x+5)+' '+str(center_y+5))
		p = Popen(cmd, shell=True)
		p.wait()
	
		####Algin croped Geotiffs
		cmd = 'pc_align --max-displacement -1 --num-iterations 0 --save-transformed-source-points --initial-transform {} {} {} -o tmp --threads 18'.format(flightID+'_ALS_transform.txt','b1_center.tif','b'+str(i+1)+'.tif')
		p = Popen(cmd, shell=True)
		p.wait()
	
	if surface_sampling == 1:
		cmd = 'point2dem tmp-trans_source.tif {} --orthoimage --nodata-value nan --threads 18 --use-surface-sampling'.format('b'+str(i+1)+'.tif')
		p = Popen(cmd, shell=True)
		p.wait()
	else:
		cmd = 'point2dem tmp-trans_source.tif {} --orthoimage --nodata-value nan --threads 18'.format('b'+str(i+1)+'.tif')
		p = Popen(cmd, shell=True)
		p.wait()

	RescaleBand('tmp-trans_source-DRG.tif','b'+str(i+1)+'_shift.tif')
	os.remove('b'+str(i+1)+'.tif')

if os.path.exists(flightName+'_orthomosaic_fr_l2.tif'):
	os.remove(flightName+'_orthomosaic_fr_l2.tif')

cmd = 'gdal_merge.py {} {} {} {} -separate -co COMPRESS=DEFLATE -co BIGTIFF=YES -co TILED=YES -o {}'.format('b1_shift.tif','b2_shift.tif','b3_shift.tif','b4_shift.tif',flightName+'_orthomosaic_fr_l2.tif')
p = Popen(cmd, shell=True)
p.wait()
cmd = 'gdal_edit.py {} -mo "AUTHOR=N. Neckel, AWI"'.format(flightName+'_orthomosaic_fr_l2.tif')
p = Popen(cmd, shell=True)
p.wait()
os.system('rm tmp* b1_shift.tif b2_shift.tif b3_shift.tif b4_shift.tif b1_center.tif')
